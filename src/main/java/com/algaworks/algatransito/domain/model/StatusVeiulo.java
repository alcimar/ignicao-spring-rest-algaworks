package com.algaworks.algatransito.domain.model;

public enum StatusVeiulo {
    REGULAR, APREENDIDO
}
